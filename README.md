[![pipeline status](https://gitlab.com/ansible-roles9/sudoers/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/sudoers/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/sudoers)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/sudoers)

# Role SUDOERS

El objetivo de este rol es el de configurar las reglas de sudo por defecto que deberían de existir en equipos perfilados como **appliance** y **normal** siguiendo el criterio establecido por la casa.

## Dependencias
Este rol no tiene dependencias de otros roles

## Funcionamiento
El funcionamiento como tal es simple, lo reflejamos en la siguiente tabla:

|  | Control de 50-sysadm | Control de 99-custom |
|--|--|--|
| Reglas custom | - | X |
| Reglas por defecto | X | - |


## Particularidades
Este rol no tiene particularidades destacables.

## Variables
Debajo una pequeña tabla que podría definir brevemente las características de cada variable:

|  | Mandatoria | Opcional |
|--|--|--|
| comment_alias | - | X |
| user_alias | - | X |
| host_alias | - | X |
| runas_alias | - | X |
| defaults | - | X |
| cmnd_alias | - | X |
| user_specifications | - | X |

### Opcionales
Las variables debajo indicadas son opcionales, en caso que no se seteen, tomarán el valor de una lista vacía, por tanto solo realizará la configuración básica de los ficheros y según sea su caso:

- **comment_alias**: Recibe el listado en formato array los siguientes parámetros:
	- **alias**: [NKS] **ALIAS**, el alias que se usará, por defecto se convertirá este **string** en mayúsculas segun reglas de SUDOERS
	- **message**: [NKS] **USUARIOS** recibe una string con un mensaje libre a elegir

- **user_alias**: Recibe el listado en formato array los siguientes parámetros:
	- **alias**: [NKS] **ALIAS**, el alias que se usará, por defecto se convertirá este **string** en mayúsculas segun reglas de SUDOERS
	- **users**: [NKS] **USUARIOS** recibe una string con los usuarios separados por ***comas***

- **host_alias**: Recibe el listado en formato array los siguientes parámetros:
	- **alias**: [NKS] **ALIAS**, el alias que se usará, por defecto se convertirá este **string** en mayúsculas segun reglas de SUDOERS
	- **hosts**: [NKS] **VALOR DE HOSTS**, por ejemplo ***10.1.2.0/255.255.255.0***

- **runas_alias**: Recibe el listado en formato array los siguientes parámetros:
	- **alias**: [NKS] **ALIAS**, el alias que se usará, por defecto se convertirá este **string** en mayúsculas segun reglas de SUDOERS
	- **users**: [NKS] **USUARIOS** recibe una string con los usuarios separados por ***comas***

- **defaults**: Recibe el listado en formato array los siguientes parámetros:
	- **user**: [NKS] **USUARIO** puede ser un usuario o un ***user_alias*** válido
	- **rule**: [NKS] **REGLA** regla ***defaults*** válida como por ejemplo ***!requiretty***

- **cmnd_alias**: Recibe el listado en formato array los siguientes parámetros:
	- **alias**: [NKS] **ALIAS**, el alias que se usará, por defecto se convertirá este **string** en mayúsculas segun reglas de SUDOERS
	- **commands**: [NKS] **COMANDOS** recibe una string separada por ***comas*** de los comandos a permitir, se deben de usar rutas absolutas

- **user_specifications**: Recibe el listado en formato array los siguientes parámetros:
	- **user**: [KS] **USUARIO** puede ser un usuario o un ***user_alias*** válido
	- **permission**: [KS] **HOST_ALIAS y RUNAS_ALIAS|USER_ALIAS** recibe una string separada por ***comas*** en formato ***[ HOST ALIAS]***,***[USER]***, si no se consigna, se usará **ALL=(root)**
	- **commands**: [NKS] **COMANDOS** puede ser un comando o un ***cmnd_alias*** válido.
	- **comment_alias**: [KS] **ALIAS DE COMENTARIO** si se declara, debe de ser uno válido antes declarado en ***comment_alias***.

Cabe destacar que cada interación en el anterior **array** será evaluada con el comando **visudo -cf**.

La sintaxis de los parámetros que recibe debería ser:
```
comment_alias:
	- {alias: 'APPCUSA', comment: 'Esta regla es para el equipo aplicativo de GDS CUSA'}
user_alias:
  - {alias: 'GL_RUNNER_USER', users: 'gitlab-runner'}
host_alias:
  - {alias: 'OFNET', hosts: '10.1.2.0/255.255.255.0'}
runas_alias:
  - {alias: 'OP', users: 'root, operator'}
defaults:
  - {user: 'GL_RUNNER_USER', rule: '!requiretty'}
cmnd_alias:
  - {alias: 'GL_RUNNER_ANSIBLE_CHK', commands: '/opt/pyenv/wan-lan/bin/ansible-playbook, /opt/pyenv/wan-lan/bin/ansible-playbook, /opt/pyenv/wan-lan/bin/python, /bin/shellcheck'}
user_specifications:
  - {user: 'GL_RUNNER_USER', permission: 'OFNET,OP', commands: 'NOPASSWD: GL_RUNNER_ANSIBLE_CHK'}
  - {user: 'juan', permission: 'OFNET,OP', commands: 'NOPASSWD: GL_RUNNER_ANSIBLE_CHK', comment_alias: 'APPCUSA'}
```

### Mandatorias
No hay variables mandatorias aunque si se declara la variable antes mencionada (custom_rules), algunas sub-variables podrían convertirse en mandatorias.

## Leyenda
* NKS => No key sensitive
* KS => Key sensitive
