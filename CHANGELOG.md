# [1.2.0](/Ansible_Roles/sudoers/compare/1.2.0...master)
* CICD migraton from Jenkins to GitlabCI

# [1.1.0](/Ansible_Roles/sudoers/compare/1.1.0...master)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se actualiza el *Jenkinsfile* para con las variables que son necesarias por la versión actual del **Shared libraries**
* Se actualiza *meta* indicando la versión de `ansible` máxima soportada.

# [1.0.0](/Ansible_Roles/sudoers/compare/1.0.0...master)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [0.3.0](/Ansible_Roles/sudoers/compare/0.3.0...master)
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla

# [0.2.0](/Ansible_Roles/sudoers/compare/0.2.0...master)
* Agregamos nuevos comandos disponibles para la regla de sudo y el usuario regular.
* Quitamos el automatismo para crear merge-request ya que el script no se encuentra activo.
* Se traduce a inglés el contenido del `README.md`
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante

# [0.1.1](/Ansible_Roles/sudoers/compare/0.1.1...master)
* Actualizamos `Jenkinsfile` para adaptarnos a la nueva versión del vars `ansibleActions`[jenkins-shared-libraries]

# [0.1.0](/Ansible_Roles/sudoers/compare/0.1.0...master)
* Se inicializa el *CHANGELOG* para un mejor seguimiento de los cambios
* Se ajusta el j2 de *50-default.j2* a fin de agregar reglas a comandos con **NOEXEC**
* Se activa el **update_cache** para cada vez que se haga la instalación de requerimientos
* Se actualiza el *Jenkinsfile* con la configuración adaptada para las nuevas librerías
* Se actualiza detalles del meta ya que contenía datos incorrectos
